﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResursObserver : MonoBehaviour
{
    [SerializeField]
    public Text Дерево;

    public ResursBanker.ResType OBSres;
    // Start is called before the first frame update
    void Start()
    {
        Дерево = GetComponent<Text>();
        ResursBanker.Instance.OnResursChanged += Newtext;
    }

    // Update is called once per frame
    void UpdateRes(ResursBanker.ResType res, int v)
    {
        Дерево.text = v.ToString();

    }

    private void Newtext(ResursBanker.ResType obj)
    {
        if (OBSres == obj)
        {
            Дерево.text = ResursBanker.Instance.GetResource(OBSres).ToString();
        };
    }
}