﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResursBanker : MonoBehaviour  
{

    public enum ResType { Рабочий, Дерево, Дом, Деньги };
    
    IDictionary<ResType, int> Resurses = new Dictionary<ResType, int>
    {
      
        { ResType.Рабочий,2},
        { ResType.Дерево,0},
        { ResType.Дом,0},
        { ResType.Деньги,0}
      
    };

    public Action<ResType> OnResursChanged; //= (ResType, v) => { };

    public static ResursBanker Instance;

    void Start()
    {
        StartCoroutine(OnChangedWood());
    }

    // Update is called once per frame
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    IEnumerator OnChangedWood()
    { while (true)
            {
            Debug.Log("kek");
            ChangeResurce(ResType.Дерево, GetResource(ResType.Рабочий));
            yield return new WaitForSeconds(0.5f);
            }
    }

    public void ChangeResurce(ResType res, int val)
    {
        Resurses[res] += val;

        OnResursChanged(res);
    }

    public int GetResource(ResType res)
    {
        
        return Resurses[res];
        
        // возвращает значение ресурса res
    }
}
