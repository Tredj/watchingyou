﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResurseExchenger : MonoBehaviour
{
    private int  a;
    private int b;
    // Start is called before the first frame update
    void Start()
    {
        ResursBanker.Instance.OnResursChanged += UP;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UP(ResursBanker.ResType obj)
    {


        if (ResursBanker.Instance.GetResource(ResursBanker.ResType.Дерево) >= 10)

        {
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Дерево, -10);
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Дом, 1);
        }
        if (ResursBanker.Instance.GetResource(ResursBanker.ResType.Дом) >= 5)

        {
            a = Random.Range(1, 5);
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Дом, -a);
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Деньги, +(100 * a));
        }
        if (ResursBanker.Instance.GetResource(ResursBanker.ResType.Деньги) >= 1000)

        {
            b = Random.Range(1, 4);
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Дерево, -(250 * b));
            ResursBanker.Instance.ChangeResurce(ResursBanker.ResType.Рабочий, +b);
        }
    }
}
