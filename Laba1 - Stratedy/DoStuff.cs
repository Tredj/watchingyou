﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoStuff : MonoBehaviour
{
    [SerializeField] GameObject go;
    [SerializeField] ParticleSystem ps;
    [Range(0, 100)] public float speed;

    private IStratagy CurrentStrategy;

    void Start()
    {
        SetStrategy(new Rotate(speed));
    }

    void Update()
    {
        CurrentStrategy?.Perform(go, ps);
         if (Input.GetKeyDown(KeyCode.Space))
         {
             SetStrategy(new Emmit());
             Debug.Log("Emmit");
         }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SetStrategy(new MoveForward(speed));
            Debug.Log("Move");
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            SetStrategy(new Rotate(speed));
            Debug.Log("Rotate");
        }

    }

    public void SetStrategy(IStratagy strats)
    {
        CurrentStrategy = strats;

    }    
}
